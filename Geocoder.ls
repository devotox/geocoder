#!/usr/bin/env node

# DO NOT USE - Geocoder.js file is created from this 
'use strict';

require! {
	lodash: \_
	bluebird: \Promise
	"request-promise": \Request
}

module.exports = class Geocoder

	defaults = 
		json: true
		max_batch: 10
		max_retries: 1
		batch_delay: 500
		max_queries: false
		account_types: [ 'practice' ] # , 'patient', 'specialist' ]
		
		maps_key: '' # 'AIzaSyA6imZYaNMlYGdGJpoCChWal9oyDeBQ5fg'
		maps_api: 'https://maps.googleapis.com/maps/api/geocode/json?'
		components: [
			{ name: 'street', return_name: 'route' }
			{ name: 'city', return_name: 'locality' }
			{ name: 'country', return_name: 'country' }
			{ name: 'premise', return_name: 'premise' }
			{ name: 'area', return_name: 'neighborhood' }
			{ name: 'postcode', return_name: 'postal_code' }
			{ name: 'number', return_name: 'street_number' }
			{ name: 'postal_town', return_name: 'postal_town' }
			{ name: 'county', return_name: 'administrative_area_level_2' }
		]
	
	(options={}) ~>
		@_errors = []
		@options = _.defaults options, defaults

	get_errors: -> @_errors

	_throw_error: (err, old_address) -> 		
		@_errors.push @_finalize_error err, old_address
		throw new Error err?.toString?!?.replace?(/error:/gi, '')?.trim?!

	# Processes a single address
	geocode: (address, callback) ->
		Promise.resolve true .bind @
		.then ->  @_throw_error "Bad Address - #{address or 'No Address'}", address unless address
		.then ->  @_geocode @_get_uri(address), address
		.then -> callback? it; it

	# Processes full list of addresses
	geocode_all: (arr, key, callback = _.identity) ->
		console.log "Processing records..."
		tasks = []; results = []; @_errors = []		
		@geocode_batch arr, key, callback, tasks .bind @
		.then -> Promise.settle(tasks).filter( ~> @_filter_results it, results ).then -> results

	# Process addresses in batch ( 50 at a time ) waiting 2 seconds in between each batch
	geocode_batch: (arr, key, callback, tasks) ->
		batch_arr = arr.splice(0, @options.max_batch)
		return Promise.resolve true if @options.max_queries and tasks.length >= @options.max_queries

		Promise.resolve true .bind @
		.then ->  
			_.each batch_arr, (address, i) ~> 
				tasks.push @geocode (if key then address[key] else address), callback
				return false if @options.max_queries and i >= @options.max_queries
		.then -> 
			Promise.settle(tasks)
		.then (result) ->
			console.log "Processed: #{tasks.length} records"

			return result unless arr.length
			Promise.delay(@options.batch_delay).bind(@)
			.then -> @geocode_batch arr, key, callback, tasks

	# Utility function which Requests Geocode and uses exponential backoff algorithm in case of over query limit errors
	_geocode: (uri, old_address, retries=0) ->
		Promise.resolve true .bind @
		.then -> Request url: uri, json: @options.json
		.then ->  @_standardize_address it, old_address
		.caught (err) -> 
			@_throw_error err, old_address if retries >= @options.max_retries
			@_backoff ( -> @_geocode uri, old_address, retries ), ++retries

	_random: ->
		Math.floor Math.random! * 3000

	_backoff: (func, retries=0) ->
		Promise.delay ( Math.pow(2, retries) * 1000 + @_random! ) .bind @ .then func
			
	_get_uri: (address='') ->
		address = address?.replace? /к/g, ''
		encodeURI "#{@options.maps_api}key=#{@options.maps_key}&address=#{address}"

	_filter_results: (address, results) ->
		switch address.isFulfilled!
			when true then results.push address.value!; true
			else false

	# Gets the best address from the pleothora of addresses returned by google
	_get_address: (address) ->
		return null unless address
		get_type = (type) -> (loc) -> loc.geometry.location_type is type

		addresses =
			rooftop: _.filter address, get_type 'ROOFTOP'
			approximate: _.filter address, get_type 'APPROXIMATE'
			geometric_center: _.filter address, get_type 'GEOMETRIC_CENTER'
			range_interpolated: _.filter address, get_type 'RANGE_INTERPOLATED'
		
		sum = _.reduce (_.keys addresses), (result, key) -> result.concat addresses[key]
		
		throw new Error "No Best Address - #{address or 'No Address'}" unless sum.length
		
		if addresses.rooftop.length
			return addresses.rooftop[0]
		else if addresses.range_interpolated.length
			return addresses.range_interpolated[0]
		else if addresses.geometric_center.length 
			return addresses.geometric_center[0] 
		else if addresses.approximate.length
			return addresses.approximate[0]

	_standardize_address: (address={}, old_address) ->
		throw new Error address.error_message if address.error_message
		throw new Error 'Bad Address - No Address Result Returned' unless address = @_get_address address?.results

		return @_finalize_address address, old_address

	# Filters the components from address_components to find the street, number, city etc.
	_filter_components: (return_name, components) ->
		found = _.filter components, (component) -> return_name in component.types
		return found?[0]?.long_name

	# Builds the Address object returned from the class
	_finalize_address: (address={}, old_address) ->
		final_address = 
			old_address: old_address
			place_id: address.place_id
			coordinates: address.geometry.location
			formatted_address: address.formatted_address
			geolocation: type: 'point', coordinates: [ address.geometry.location?.lng, address.geometry.location?.lat ]

		_.each @options.components, (component) ~>
			final_address[component.name] = ( @_filter_components component.return_name, address.address_components ) or ''
		
		return final_address

	_finalize_error: (err, old_address) ->
		error: err?.toString?!
		old_address: old_address
		uri: @_get_uri old_address

