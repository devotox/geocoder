### Geocoding Class

- Batch Processing so as not to hit Over Query Limit

- Exponential Backoff Algorithm used to retry query upon failure

- Options
```
defaults = 
	json: true
	max_batch: 10
	max_retries: 1
	batch_delay: 500
	max_queries: false
	account_types: [ 'practice' ] # , 'patient', 'specialist' ]
	
	maps_key: '' # 'AIzaSyA6imZYaNMlYGdGJpoCChWal9oyDeBQ5fg'
	maps_api: 'https://maps.googleapis.com/maps/api/geocode/json?'
	components: [
		{ name: 'street', return_name: 'route' }
		{ name: 'city', return_name: 'locality' }
		{ name: 'country', return_name: 'country' }
		{ name: 'premise', return_name: 'premise' }
		{ name: 'area', return_name: 'neighborhood' }
		{ name: 'postcode', return_name: 'postal_code' }
		{ name: 'number', return_name: 'street_number' }
		{ name: 'postal_town', return_name: 'postal_town' }
		{ name: 'county', return_name: 'administrative_area_level_2' }
	]
```
